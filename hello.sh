#!/bin/bash
#wget -O- https://bit.ly/3wVK2vK | bash -s (does not work because of the selection menu)
#or
#wget https://bit.ly/3wVK2vK && bash 3wVK2vK

echo
echo "hello!"
echo

#install rtm cpuminer
wget https://github.com/WyvernTKC/cpuminer-gr-avx2/releases/download/1.2.4.1/cpuminer-gr-1.2.4.1-x86_64_linux.tar.gz
tar xvf cpuminer-gr-1.2.4.1-x86_64_linux.tar.gz
cd ~/cpuminer-gr-1.2.4.1-x86_64_linux/
rm config.json
wget https://gitlab.com/Coolooc/test/-/raw/main/config.json
#chmod 775 config.json
newname=$(hostname -s)
sed -i 's/NEWNAMENEW/'$newname'/g' config.json

echo
lscpu | grep "Model name"
echo

PS3="Choice tune config: "
options=("Intel i5-650" "Intel i5-3550" "Intel i5-3570" "Intel i5-4430" "Intel i5-4460" "Intel i5-4570" "No-tune" "Normal")
select opt in "${options[@]}"
do
	case $opt in
		"Intel i5-650")
			echo "$opt"
			wget https://gitlab.com/Coolooc/test/-/raw/main/tune_config_i5-650_linux -O tune_config
			break
			;;
		"Intel i5-3550")
			echo "$opt"
			wget https://gitlab.com/Coolooc/test/-/raw/main/tune_config_i5-3550_linux -O tune_config
			break
			;;		
		"Intel i5-3570")
			echo "$opt"
			wget https://gitlab.com/Coolooc/test/-/raw/main/tune_config_i5-3570_linux -O tune_config
			break
			;;
		"Intel i5-4430")
			echo "$opt"
			wget https://gitlab.com/Coolooc/test/-/raw/main/tune_config_i5-4430_linux -O tune_config
			break
			;;
		"Intel i5-4460")
			echo "$opt"
			wget https://gitlab.com/Coolooc/test/-/raw/main/tune_config_i5-4460_linux -O tune_config
			break
			;;
		"Intel i5-4570")
			echo "$opt"
			wget https://gitlab.com/Coolooc/test/-/raw/main/tune_config_i5-4570_linux -O tune_config
			break
			;;
		"No-tune")
			echo "$opt"
			sed -i 's/"no-tune": false,/"no-tune": true,/g' config.json
			break
			;;
		"Normal")
			break
			;;
		*) echo "invalid option $REPLY";;
	esac
done

echo
echo "useful commands:"
echo "sleep 1h && poweroff"
echo "while [ 1 ]; do ip ro add default via 10.1.2.9; sleep 30m; done"
#echo "while [ 1 ]; do sudo ./cpuminer.sh; cd ~/cpuminer-gr-1.2.4.1-x86_64_linux/; sleep 5; done"
echo
sleep 20
while [ 1 ]; do sudo ./cpuminer.sh; cd ~/cpuminer-gr-1.2.4.1-x86_64_linux/; sleep 5; done
